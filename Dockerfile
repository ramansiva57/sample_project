FROM alpine:3.7
LABEL Aathi <aathi@techardors.com>

RUN apk update
RUN apk add nodejs 
RUN apk add nginx
RUN set -x ; \
  addgroup -g 82 -S www-data ; \
  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

COPY ./nginx.conf	    /etc/nginx/nginx.conf
#COPY ./localhost.crt	/etc/nginx/localhost.crt
#COPY ./localhost.key	/etc/nginx/localhost.key
COPY ./code/ecom_front_proj /sections
WORKDIR sections
RUN npm install
RUN npm install -g @angular/cli
RUN ng build --prod


